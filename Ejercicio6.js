var jugados = 0;
    var ganados = 0;
    var perdidos = 0;
    var gParcial = 0;
    var pParcial = 0;
    var jugarDeNuevo = 1;

    while (jugarDeNuevo) {
        var jugada = prompt("Escribe \"piedra\", \"papel\" o \"tijera\"");
        
        // GUARDAMOS UN NÚMERO ALEATORIO DE 1 AL 9
        var aleatorio = Math.floor((Math.random() * 9) + 1);
        var elige = "";
        if (aleatorio <= 3) {
            elige = "piedra";
        }
        else if (aleatorio <= 6) {
            elige = "papel";
        }
        else {
            elige = "tijera";
        }

	     // "jugada" es la persona, "elige" la máquina
         if ((jugada == "piedra") && (elige == "piedra")) {
            alert(jugada+"-"+elige+": Empate");
        }
        else if ((jugada == "piedra") && (elige == "papel")) {
            alert(jugada+"-"+elige+": Gana PC");
            perdidos++;
            pParcial++;
        }
        else if ((jugada == "piedra") && (elige == "tijera")) {
            alert(jugada+"-"+elige+": Gana Jugador");
            ganados++;
            gParcial++;
        }
        else if ((jugada == "papel") && (elige == "papel")) {
            alert(jugada+"-"+elige+": Empate");
        }
        else if ((jugada == "papel") && (elige == "tijera")) {
            alert(jugada+"-"+elige+": Gana PC");
            perdidos++;
            pParcial++;
        }
        else if ((jugada == "papel") && (elige == "piedra")) {
            alert(jugada+"-"+elige+": Gana Jugador");
            ganados++;
            gParcial++;
        }
        else if ((jugada == "tijera") && (elige == "tijera")) {
            alert(jugada+"-"+elige+": Empate");
        }
        else if ((jugada == "tijera") && (elige == "piedra")) {
            alert(jugada+"-"+elige+": Gana PC");
            perdidos++;
            pParcial++;
        }
        else if ((jugada == "tijera") && (elige == "papel")) {
            alert(jugada+"-"+elige+": Gana Jugador");
            ganados++;
            gParcial++;
        }
        jugados++;

        if (pParcial >= 3){
            jugarDeNuevo = 0;
            pParcial = 0;
            gParcial = 0;
            if (confirm("Has perdido 3 partidas, ¿seguir?")) {
                jugarDeNuevo = 1;
            }
        }

        if (gParcial >= 3){
            jugarDeNuevo = 0;
            gParcial = 0;
            pParcial = 0;
            if (confirm("Has ganado 3 partidas, ¿seguir?")) {
                jugarDeNuevo = 1;
            }
        }
    };

    document.write("Partidas jugadas: "+jugados+"<br>");
    document.write("Partidas ganadas: "+ganados+"<br>");
    document.write("Partidas perdidas: "+perdidos+"<br>");
    document.write("Partidas empatadas: "+(jugados-ganados-perdidos));